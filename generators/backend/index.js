const Generator = require('yeoman-generator');

let serverModules = [
  'server_modules/language/get-language-pack.js',
  'server_modules/language/sv_SE.json',
  'server_modules/models/index.js',
  'server_modules/models/user.js',
  'server_modules/sessions/index.js',
  'server_modules/config-template.json',
  'server_modules/users.js'
];

let routes = [
  'routes/index.js',
  'routes/login.js',
  'routes/users.js'
];

let migrations = [
  'migrations/create-user.js'
];

let root = [
  'app.js',
  'gulpfile.js',
  'package.json'
];

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);
  }

  writing() {
    let path = this.config.get('newFolder') ? `${this.config.get('projectName')}/backend` : 'backend';
    this.log(`Writing backend files to ${path}`);

    const copyFile = file => {
      this.fs.copyTpl(
        this.templatePath(file),
        this.destinationPath(`${path}/${file}`),
        { title: this.config.get('projectName') }
      );
    };

    serverModules.forEach(copyFile);
    routes.forEach(copyFile);
    migrations.forEach(copyFile);
    root.forEach(copyFile);
  }

  install() {
    let path = this.config.get('newFolder') ? `${this.config.get('projectName')}/backend` : 'backend';

    this.yarnInstall(null, null, null, {
      cwd: path
    });
  }
};
