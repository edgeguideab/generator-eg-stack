var gulp = require('gulp');
var fs = require('fs');
var environment = 'dev';

gulp.task('build', function() {
  let appConfigParameters = {
    appConfigPath: `./server_modules/config-${environment}.json`,
    appConfigTemplatePath: './server_modules/config-template.json',
    targetPath: './server_modules/config.json'
  };
  if (!createAppConfig(appConfigParameters)) {
    console.error('BUILD FAILED');
    process.exit(1);
    return;
  }
});

gulp.task('default', ['build'], function() {});

function createAppConfig({appConfigPath, appConfigTemplatePath, targetPath}) {
  let appConfig = require(appConfigPath);
  let appConfigTemplate = require(appConfigTemplatePath);
  let success = checkObjectEquivalency(appConfig, appConfigTemplate);
  if (success) {
    fs.writeFileSync(targetPath, fs.readFileSync(appConfigPath));
  }
  return success;

  function checkObjectEquivalency(objectToCheck, templateObject) {
    let success = true;
    Object.keys(templateObject).forEach(templateKey => {
      if ((typeof objectToCheck[templateKey] !== typeof templateObject[templateKey])) {
        console.log(`Key ${templateKey} had different types in template and ${environment} (${environment} had ${typeof objectToCheck[templateKey]})`);
        success = false;
        return;
      }
      if (Array.isArray(objectToCheck[templateKey]) !== Array.isArray(templateObject[templateKey])) {
        console.error(`Key ${templateKey} had different types in template and ${environment} (one is list and the other not)`);
        success = false;
        return;
      }
      if (typeof objectToCheck[templateKey] === 'object' && objectToCheck[templateKey] !== null) {
        let subObjectSuccess = checkObjectEquivalency(objectToCheck[templateKey], templateObject[templateKey]);
        if (!subObjectSuccess) {
          success = false;
        }
      }
    });
    return success;
  }
}
