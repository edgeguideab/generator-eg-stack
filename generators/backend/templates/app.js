const config = require('app/config.json');
const logger = require('winston');
logger.add(logger.transports.File, {filename: './logs/server.log'});
logger.level = config.logger.level;
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const sessions = require('client-sessions');
const crypto = require('crypto');
const port = 3100;
const devMode = config.environment === 'dev';
const sessionSecret = devMode ? 'hardcodedDevSecret' : crypto.randomBytes(100).toString('base64');


app.use(sessions({
  cookieName: 'session', // cookie name dictates the key name added to the request object
  secret: sessionSecret,
  duration: 10 * 60 * 1000,
  activeDuration: 5 * 60 * 1000 * 60
}));

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', config.clientDomain);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Cookie');
  res.header('Access-Control-Allow-Credentials', true);
  next();
};

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json({
  limit: '10mb'
}));
app.use(bodyParser.raw({
  limit: '10mb'
}));

const routes = require('./routes');

app.use(allowCrossDomain);
app.use('/', routes);

app.listen(port, () => logger.info(`Server starting on ${port}`));
