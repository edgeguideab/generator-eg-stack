const express = require('express');
const router = express.Router();
const users = require('./users');
const getLanguagePack = require('app/language/get-language-pack');
const login = require('./login');

router.get('/hello-world', (req, res) => {
  res.send('Hello.');
});

router.get('/language', getLanguagePack);

router.use('/users', users);

router.use('/sessions', login);


module.exports = router;
