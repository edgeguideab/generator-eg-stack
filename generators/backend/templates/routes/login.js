const express = require('express');
const router = express.Router();

const sessions = require('app/sessions');

router.post('/login', sessions.login);
router.post('/logout', sessions.logout);

module.exports = router;
