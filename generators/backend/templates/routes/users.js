const express = require('express');
const router = express.Router();

const users = require('app/users');

router.get('/:username', users.getUser);
router.put('/', users.createUser);

module.exports = router;
