'use strict';
var fs = require('fs');
var path = require('path');
var logger = require('winston');
var Sequelize = require('sequelize');
var basename = path.basename(module.filename);
var config = require('app/config.json');

var sequelizeConfig = config.sqlConfig;
sequelizeConfig.logging = null;
if (config.logger.logDangerousInformation.enabled) {
  sequelizeConfig.logging = logger[config.logger.logDangerousInformation.level];
}
var sequelize = new Sequelize(sequelizeConfig.database, sequelizeConfig.username, sequelizeConfig.password, sequelizeConfig);

var db = {};
fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
