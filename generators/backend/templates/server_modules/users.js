const logger = require('winston');
const db = require('app/models');
const expect = require('@edgeguide/expect');
const bcryptjs = require('bcryptjs');

function getUser(req, res) {
  logger.silly('Running getUser');
  let userId = req.params.username;
  logger.silly(`userId=${userId}`);

  if (!userId) {
    res.status(400).send('Missing userId');
    return;
  }

  db.User.findOne({
    where: {
      username: userId
    }
  }).then(sendUser)
  .catch(sendInvalidResponse);

  function sendUser(user) {
    if (!user) {
      logger.silly(`querying user with id ${userId} resulted in ${user}`);
      sendInvalidResponse();
      return;
    }
    res.send(user);
  }

  function sendInvalidResponse(err) {
    logger.silly(err);
    res.status(400).send('Invalid userId');
  }
}

function createUser(req, res) {
  logger.silly('running createUser');
  let {username, password} = req.body;
  let expectations = expect({
    username,
    password
  }, {
    username: 'string',
    password: 'string'
  });

  if (!expectations.wereMet()) {
    res.status(400).send(expectations.errors());
    return;
  }

  let hash = bcrypt.hashSync(password, 10);
  db.User.create({
    username: username,
    password: hash
  })
  .then(() => res.status(201).send())
  .catch(() => res.status(500).send());
}

module.exports = {
  getUser,
  createUser
};
