const languages = {
  'sv_SE': require('./sv_SE')
};


module.exports = function(req, res) {
  let {languageCode} = req.query;  
  languageCode = languageCode || 'sv_SE';

  res.send(languages[languageCode]);
};
