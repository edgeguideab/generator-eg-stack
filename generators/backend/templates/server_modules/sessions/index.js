const logger = require('winston');
const expect = require('@edgeguide/expect');
const db = require('app/models');
const moment = require('moment');

const login = (req, res) => {
  let password = req.body.password;
  let username = req.body.username;

  expect({
    password,
    username
  }, {
    password: 'string',
    username: 'string'
  });

  db.User.findOne({
    where: {
      username: username
    }
  }).then(user => {
    if (!user) {
      res.status(404).send({
        msg: 'noUserFound'
      });
      return;
    }
    if (bcrypt.compareSync(password, user.password)) {
      req.session.user = username;
      res.send({
        msg: 'authenticationSuccess'
      });
    } else {
      res.status(401).send({
        msg: 'incorrectCredentials'
      });
    }
  }).catch(error => {
    logger.error(error);
    res.status(400).send({
      msg: 'errors.databaseError'
    });
  });
};

const restrict = (req, res, next) => {
  if (req.session.user) {
    next();
    return;
  }

  res.status(401).send();
};

const logout = (req, res) => {
  delete req.session.user;
  res.status().send();
};

module.exports = {
  login,
  logout
};
