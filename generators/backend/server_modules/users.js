const logger = require('winston');
const db = require('app/models');
const expect = require('@edgeguide/expect');

function getUser(req, res) {
  logger.silly('Running getUser');
  let userId = req.params.id;
  logger.silly(`userId=${userId}`);

  if (!userId) {
    res.status(400).send('Missing userId');
    return;
  }

  db.User.findOne({
    where: {
      id: userId
    },
    include: [db.Organization]
  }).then(sendUser)
  .catch(sendInvalidResponse);

  function sendUser(user) {
    if (!user) {
      logger.silly(`querying user with id ${userId} resulted in ${user}`);
      sendInvalidResponse();
      return;
    }
    res.send(user);
  }

  function sendInvalidResponse(err) {
    logger.silly(err);
    res.status(400).send('Invalid userId');
  }
}

function createUser(req, res) {
  logger.silly('running createUser');
  let {username, password} = req.body;
  let expectations = expect({
    username,
    password
  }, {
    username: 'string',
    password: 'string'
  });

  if (!expectations.wereMet()) {
    res.status(400).send(expectations.errors());
    return;
  }

  let hash = encrypter.hashPersonalIdentityNumber(personalIdentityNumber);
  db.User.create({personalIdentityNumber: hash, organizationId})
  .then(() => res.status(201).send())
  .catch(() => res.status(500).send());
}

module.exports = {
  getUser,
  createUser
};
