const logger = require('winston');
const expect = require('@edgeguide/expect');
const db = require('app/models');
const moment = require('moment');

const login = (req, res) => {
  let password = req.body.password;
  let username = req.body.username;

  expect({
    password,
    username
  }, {
    password: 'string',
    username: 'string'
  });

  if (password === 'test' && username === 'dummy') {
    req.session.user = 'dummy';
    res.send({
      msg: 'authenticationSuccess'
    });
  } else {
    res.status(401).send({
      msg: 'incorrectCredentials'
    });
  }
};

const restrict = (req, res, next) => {
  if (req.session.user) {
    next();
    return;
  }

  res.status(401).send();
};

const logout = (req, res) => {
  delete req.session;
  res.status().send();
};

module.exports = {
  login,
  logout
};
