const Generator = require('yeoman-generator');

const jsFiles = [
  'js/actions/session.js',
  'js/components/views/LoginView/LoginForm.jsx',
  'js/components/views/LoginView/LoginView.jsx',
  'js/components/views/view-components/Header.jsx',
  'js/components/views/view-components/SiteFooter.jsx',
  'js/containers/LoginContainer.js',
  'js/reducers/index.js',
  'js/reducers/session.js',
  'js/themes/main.js',
  'js/utils/constants.json',
  'js/utils/language.js',
  'js/utils/urls.json',
  'js/.eslintrc.json',
  'js/ApplicationRoot.jsx',
  'js/config-template.json',
  'js/package.json',
  'lib/vendor/promise-7.0.4.min.js'
];

const styles = [
  'styles/typography.scss',
  'styles/index.scss',
  'styles/global.scss',
  'styles/mixins/index.scss',
  'styles/constants/breakpoints.scss',
  'styles/constants/colors.scss',
  'styles/constants/index.scss'
];

const config = [
  'gulpfile.js',
  'index.html',
  'package.json',
  '.gitignore'
];

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);
  }

  writing() {
    let path = this.config.get('newFolder') ? `${this.config.get('projectName')}/frontend` : 'frontend';
    this.log(`Writing frontend files to ${path}`);

    const copyFile = file => {
      this.fs.copyTpl(
        this.templatePath(file),
        this.destinationPath(`${path}/${file}`),
        { title: this.config.get('projectName') }
      );
    };

    jsFiles.forEach(copyFile);
    styles.forEach(copyFile);
    config.forEach(copyFile);
  }

  install() {
    let path = this.config.get('newFolder') ? `${this.config.get('projectName')}/frontend` : 'frontend';

    this.yarnInstall(null, null, null, {
      cwd: path
    });
  }
};
