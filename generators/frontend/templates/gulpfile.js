var gulp = require('gulp');
var fs = require('fs');
var connect = require('gulp-connect');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var gulpif = require('gulp-if');
var sass = require('gulp-ruby-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var path = require('path');
var environment = 'dev';


gulp.task('build', function() {
  let appConfigParameters = {
    appConfigPath: `./js/config-${environment}.json`,
    appConfigTemplatePath: './js/config-template.json',
    targetPath: './js/config.json'
  };
  if (!createAppConfig(appConfigParameters)) {
    console.error('BUILD FAILED');
    process.exit(1);
    return;
  }
  return browserify('./js/ApplicationRoot.jsx', {
      paths: ['./node_modules', '.']
    }).transform(babelify.configure({
      presets: ['es2015', 'react', 'stage-2']
    }))
    .bundle()
    .on('error', onError)
    .pipe(source('app.js'))
    .pipe(gulp.dest('build'));
});

function createAppConfig({appConfigPath, appConfigTemplatePath, targetPath}) {
  let appConfig = require(appConfigPath);
  let appConfigTemplate = require(appConfigTemplatePath);
  let success = checkObjectEquivalency(appConfig, appConfigTemplate);
  if (success) {
    fs.writeFileSync(targetPath, fs.readFileSync(appConfigPath));
  }
  return success;

  function checkObjectEquivalency(objectToCheck, templateObject) {
    let success = true;
    Object.keys(templateObject).forEach(templateKey => {
      if ((typeof objectToCheck[templateKey] !== typeof templateObject[templateKey])) {
        console.log(`Key ${templateKey} had different types in template and ${environment} (${environment} had ${typeof objectToCheck[templateKey]})`);
        success = false;
        return;
      }
      if (Array.isArray(objectToCheck[templateKey]) !== Array.isArray(templateObject[templateKey])) {
        console.error(`Key ${templateKey} had different types in template and ${environment} (one is list and the other not)`);
        success = false;
        return;
      }
      if (typeof objectToCheck[templateKey] === 'object') {
        let subObjectSuccess = checkObjectEquivalency(objectToCheck[templateKey], templateObject[templateKey]);
        if (!subObjectSuccess) {
          success = false;
        }
      }
    });
    return success;
  }
}

gulp.task('serve', () => {
  connect.server({
    name: '<%= title %>',
    root: ['.'],
    fallback: './index.html',
    port: 3180
  });
});

gulp.task('watch', () => {
  gulp.watch(['js/**/*.js', 'js/**/*.jsx'], ['build']);
  gulp.watch('styles/**/*.scss', ['styles']);
});

gulp.task('default', () => {
  gulp.start('build', 'styles', 'watch');
});

gulp.task('styles', function() {
  return sass('styles/index.scss')
    .pipe(plumber())
    .pipe(concat('app.css'))
    .pipe(autoprefixer())
    .pipe(gulpif(environment === 'prod', cssnano({
      zindex: false
    })))
    .pipe(gulp.dest('build', {
      overwrite: true
    }));
});

gulp.task('prod', function() {
  environment = 'prod';
  gulp.start('build', 'styles');
});

function onError(err) {
  console.log(err.toString());
  this.emit('end');
}
