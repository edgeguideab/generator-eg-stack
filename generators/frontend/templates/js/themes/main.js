const colors = require('material-ui/styles/colors');
const colorManipulator = require('material-ui/utils/colorManipulator');
const spacing = require('material-ui/styles/spacing');
const _interopRequireDefault = (obj) => { return obj && obj.__esModule ? obj : { default: obj }; };
const spacing2 = _interopRequireDefault(spacing);

const constants = require('app/utils/constants');

module.exports = {
  spacing: spacing2.default,
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: constants.primaryColor,
    primary2Color: colors.red700,
    primary3Color: colors.grey400,
    accent1Color: constants.secondaryColor,
    accent2Color: colors.grey100,
    accent3Color: colors.grey500,
    textColor: colors.darkBlack,
    secondaryTextColor: (0, colorManipulator.fade)(colors.darkBlack, 0.54),
    alternateTextColor: colors.white,
    canvasColor: colors.white,
    borderColor: colors.grey300,
    disabledColor: (0, colorManipulator.fade)(colors.darkBlack, 0.3),
    pickerHeaderColor: colors.primaryColor,
    clockCircleColor: (0, colorManipulator.fade)(colors.darkBlack, 0.07),
    shadowColor: colors.fullBlack
  }
};
