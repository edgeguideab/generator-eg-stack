var React = require('react');
var ReactDOM = require('react-dom');
var {List, Map, fromJS} = require('immutable');
const translate = require('app/utils/language').translate;
const TextField = require('material-ui/TextField').default;
const RaisedButton = require('material-ui/RaisedButton').default;

const ErrorText = ({msg}) => {
  msg = msg ? translate(msg) : '';
  var className = msg ? 'visible' : '';

  return (
    <p className={`error-text ${className}`}>{msg}</p>
  );
};

const LoginForm = ({onSubmit, error, onInputChange, data, onKeyDown}) => {
  var personalIdentityNumber = data && data.get('personalIdentityNumber') || '';

  return (
    <form className='login-form' onSubmit={onSubmit}>
      <div>
        <TextField
          floatingLabelText={translate('personalIdentityNumber')}
          value={personalIdentityNumber}
          name='personalIdentityNumber'
          onChange={onInputChange}
          fullWidth={true}/>
      </div>
      <div className='login-button-row'>
        <ErrorText msg={translate(error)}/>
        <RaisedButton type='submit' className='psn-button' onClick={onSubmit} label={translate('login')} secondary={true}/>
      </div>
    </form>
  );
};

module.exports = LoginForm;
