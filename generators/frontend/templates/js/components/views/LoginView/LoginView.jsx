const React = require('react');
const ReactDOM = require('react-dom');
const ReactRedux = require('react-redux');
const LoginForm = require('app/components/views/LoginView/LoginForm.jsx');
const Header = require('app/components/views/view-components/Header.jsx');
const SiteFooter = require('app/components/views/view-components/SiteFooter.jsx');
const translate = require('app/utils/language').translate;


const LoginView = ({login, loginError, onInputChange, loginForm, onKeyDown, fetching, loginMessage, loggedIn}) => {
  return (
    <div className='view login-view'>
      <Header />
      <div className='login-view-content'>
        <div className='left-column'>
          <h1>{translate('loginView.title')}</h1>
          <LoginForm onSubmit={login}
            error={loginError}
            onInputChange={onInputChange}
            data={loginForm}
            onKeyDown={onKeyDown}
            loader={fetching}
            message={loginMessage}
            error={loginError} />
        </div>
      </div>
      <SiteFooter />
    </div>
  );
};

module.exports = LoginView;
