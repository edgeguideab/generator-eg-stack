const React = require('react');
const ReactDOM = require('react-dom');
const translate = require('app/utils/language').translate;

const Footer = () => {
  return <footer id='site-footer'>{translate('footer.siteName')}</footer>;
};

module.exports = Footer;
