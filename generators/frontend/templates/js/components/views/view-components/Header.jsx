const React = require('react');
const ReactDOM = require('react-dom');
const translate = require('app/utils/language').translate;

const Header = () => {
  return <header id='site-header'>{translate('header.siteName')}</header>;
};

module.exports = Header;
