const translate = key => {
  if (window.LanguageKeys) {
    if (!key || typeof key !== 'string') {
      return '';
    }
    let keyChain = key.split('.');
    let result = null;
    keyChain.forEach(key => {
      if (result) {
        result = result[key];
      } else {
        result = window.LanguageKeys[key];
      }
    });
    return result || key;
  } else {
    return key;
  }
};

module.exports = {
  translate
};
