const config = require('app/config.json');
const ReactDOM = require('react-dom');
const React = require('react');
const Redux = require('redux');
const thunk = require('redux-thunk').default;
const ReactRedux = require('react-redux');
const {Router, Route, browserHistory, IndexRedirect, IndexRoute} = require('react-router');
const combinedReducers = require('app/reducers');
var {syncHistoryWithStore, routerMiddleware, push, replace} = require('react-router-redux');
const LoginContainer = require('app/containers/LoginContainer');
const request = require('@edgeguide/client-request');
const injectTapEventPlugin = require('react-tap-event-plugin');
const MuiThemeProvider = require('material-ui/styles/MuiThemeProvider').default;
const getMuiTheme = require('material-ui/styles/getMuiTheme').default;
const mainTheme = require('app/themes/main');
const urls = require('app/utils/urls.json');

injectTapEventPlugin();

let devToolsMiddleware = window.devToolsExtension ? window.devToolsExtension() : f => f;
routerMiddleware = routerMiddleware(browserHistory);
let middlewares = Redux.compose(Redux.applyMiddleware(thunk), Redux.applyMiddleware(routerMiddleware), devToolsMiddleware);
let store = Redux.createStore(combinedReducers, {}, middlewares);
var Provider = ReactRedux.Provider;

const history = syncHistoryWithStore(browserHistory, store);

const requireAuth = (nextState, replace) => {
  if (!store.getState().session.get('loggedIn')) {
    replace({
      pathname: '/login'
    });
  }
};

request.get(`${config.apiServer}/language`, {
  languageCode: 'sv_SE'
})
  .then(response => {
    window.LanguageKeys = response.data;
    ReactDOM.render(
      <MuiThemeProvider muiTheme={getMuiTheme(mainTheme)}>
        <Provider store={store}>
          <Router history={history}>
            <Route path='/'>
              <IndexRoute component={LoginContainer}/>
              <Route path={urls.login} component={LoginContainer}/>
              <Route path='*' component={LoginContainer} />
            </Route>
          </Router>
        </Provider>
      </MuiThemeProvider>, document.getElementById('react-root'));
  }).catch(err => {
    if (console && console.error) {
      console.error(err);
    }
    ReactDOM.render(<div>Server error</div>, document.getElementById('react-root'));
  });
