var ReactRedux = require('react-redux');
var React = require('react');
var sessionActions = require('app/actions/session.js');
var LoginView = require('app/components/views/LoginView/LoginView.jsx');

const mapStateToProps = state => {
  return {
    loginError: state.session.get('error'),
    loginForm: state.session.get('loginForm'),
    fetching: state.session.get('isFetching'),
    loginMessage: state.session.get('message'),
    loggedIn: state.session.get('hasAuthenticated'),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: e => {
      e.preventDefault();
      document.querySelector('.login-form input').blur();
      dispatch(sessionActions.doLogin());
      return false;
    },
    onInputChange: e => {
      let value = e.currentTarget.value;
      dispatch(sessionActions.inputChange(value));
    }
  };
};

var LoginContainer = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(LoginView);

module.exports = LoginContainer;
