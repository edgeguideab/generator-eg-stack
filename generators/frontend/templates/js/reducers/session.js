var Immutable = require('immutable');

const sessions = (state, action) => {
  state = state || Immutable.fromJS({
    isFetching: false,
    loginForm: {
      personalIdentityNumber: ''
    },
    user: {
      id: ''
    },
    bankIdError: '',
    message: '',
    error: '',
    hasAuthenticatedWithBankid: false,
  });
  switch (action.type) {
    case 'SESSION_LOGIN_FORM_CHANGE': {
      return state.setIn(['loginForm', 'personalIdentityNumber'], action.payload);
    }
    case 'SESSION_LOGIN_STARTED': {
      return state.set('error', '')
                  .set('message', '')
                  .set('bankIdError', '');
    }
    case 'SESSION_LOGIN_START_LOADER': {
      return state.set('isFetching', true);
    }
    case 'SESSION_LOGIN_PENDING': {
      return state.set('message', action.payload.msg);
    }
    case 'SESSION_LOGIN_DONE': {
      return state.set('error', '')
                  .set('message', action.payload.msg)
                  .set('hasAuthenticatedWithBankid', true)
                  .set('isFetching', false);
    }
    case 'SESSION_LOGIN_FAILED': {
      let newState = state;
      if (!action.payload.badInput && action.payload.msg !== 'bankid.personalIdentityNumberNotFound') {
        newState = newState.set('bankIdError', action.payload.msg);
      } else {
        newState = newState.set('error', action.payload.msg);
      }
      return newState.set('message', '')
                  .set('isFetching', false);
    } case 'SESSION_BANKID_MODAL_CLOSE': {
      return state.set('bankIdError', '');
    }
    default:
      return state;
  }
};

module.exports = sessions;
