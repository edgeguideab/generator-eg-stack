const redux = require('redux');
const { routerReducer } = require('react-router-redux');
const sessionReducer = require('./session');

const allReducers = redux.combineReducers({
  routing: routerReducer,
  session: sessionReducer
});


module.exports = allReducers;
