var Generator = require('yeoman-generator');

module.exports = class extends Generator {

  constructor(args, opts) {
    super(args, opts);
  }

  initializing() {
    this.composeWith(require.resolve('../frontend'));
    this.composeWith(require.resolve('../backend'));
  }

  prompting() {
    return this.prompt([{
      type    : 'input',
      name    : 'projectName',
      message : 'Enter your project name',
      default : this.appname // Default to current folder name
    }, {
      type    : 'confirm',
      name    : 'newFolder',
      message : 'Do you want to create a new folder for the project?'
    }]).then((answers) => {
      this.log('Project name:', answers.projectName);
      this.log('Create project in folder:', answers.newFolder ? this.appname : answers.projectName);
      this.config.set('projectName', answers.projectName);
      this.config.set('newFolder', answers.newFolder);
      this.config.save();
    });
  }
};
